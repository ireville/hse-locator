﻿using System;
namespace BotLib
{
    public class RoutRequest
    {
        private string waypoint0;
        private string waypoint1;

        public RoutRequest(string waypoint0, string waypoint1)
        {
            this.waypoint0 = ChangeToPoint(waypoint0);
            this.waypoint1 = ChangeToPoint(waypoint1);
        }

        public override string ToString()
        {
            return "https://route.api.here.com/routing/7.2/calculateroute.json?waypoint0="
                + waypoint0 + "&waypoint1="+ waypoint1 +
                "&mode=fastest%3Bcar%3Btraffic%3Aenabled&app_id=WCDkSn40b1i1zYNWcW8R&app_code=-7NAhxCWMr-aLzq_09pECg";
        }

        public static string ChangeToPoint(string coord)
        {
            string[] words = new string[2];
            for (int i = 0; i < coord.Length; i++)
            {
                words = coord.Split(',', StringSplitOptions.RemoveEmptyEntries);
            }
            return words[0] + "%2C" + words[1];
        }
    }   
}
