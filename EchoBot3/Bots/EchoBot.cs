// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EchoBot v4.6.2

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using BotLib;
using System;

namespace EchoBot3.Bots
{
    public class EchoBot : ActivityHandler
    {
        static string adress1, adress2;
        static int cnt = 1;

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            switch (cnt)
            {
                case 1:

                    await turnContext.SendActivityAsync(CreateActivityWithTextAndSpeak($"�������� ����� �����������, ���� ������� ����� ����."), cancellationToken);
                    cnt++;
                    await AskComplexAsync(turnContext, cancellationToken);
                    break;

                case 2:
                    cnt++;
                    await ChooseAdresses(turnContext, cancellationToken);
                    break;

                case 3:
                    adress1 = turnContext.Activity.Text;

                    await turnContext.SendActivityAsync(CreateActivityWithTextAndSpeak($"������ �������� ����� ��������, ���� ������� ����� ����."), cancellationToken);
                    cnt++;
                    await AskComplexAsync(turnContext, cancellationToken);
                    break;
                case 4:
                    goto case 2;
                case 5:
                    adress2 = turnContext.Activity.Text;
                    var reply = ProcessInput(turnContext);
                    await turnContext.SendActivityAsync(reply, cancellationToken);
                    await AskComplexAsync(turnContext, cancellationToken);
                    cnt = 2;
                    break;
            }
        }

        private static IMessageActivity HandleOutgoingAttachment(ITurnContext turnContext, IMessageActivity activity, string firstCoord, string secondCoord)
        {
            // Look at the user input, and figure out what kind of attachment to send.
            IMessageActivity reply = null;

            reply = MessageFactory.Text("���� ���� ��������: ");
            reply.Attachments = new List<Attachment>() { GetInternetAttachment(firstCoord, secondCoord) };


            return reply;
        }
        private static Attachment GetInternetAttachment(string firstCoord, string secondCoord)
        {
            string url = "";
            try
            {
                var json = Search.GetJson(adress1);
                firstCoord = Search.GetCoords(json);
                json = Search.GetJson(secondCoord);
                secondCoord = Search.GetCoords(json);
                var newJson = ParseMethods.GetJson(firstCoord, secondCoord);
                var result = ParseMethods.GetPoints(newJson);
                var startAndEnd = ParseMethods.GetStartAndEndPoints(result);
                url = ParseMethods.GetLink(result, startAndEnd);
            }
            catch (Exception)
            {
                return new Attachment
                {
                    Name = @$"�� {adress1} �� {adress2}.png",
                    ContentType = "image/png",
                    ContentUrl = "https://avatars.mds.yandex.net/get-pdb/1004346/4d3cc5a6-5c44-4fd4-9bc3-a8efedfbd243/s1200?webp=false",
                };
            }

            return new Attachment
            {
                Name = @"Maps.png",
                ContentType = "image/png",
                ContentUrl = url,
            };

        }
        private static IMessageActivity ProcessInput(ITurnContext turnContext)
        {
            var activity = turnContext.Activity;
            IMessageActivity reply = null;
            // Send at attachment to the user.
            reply = HandleOutgoingAttachment(turnContext, activity, adress1, adress2);
            return reply;
        }

        /// <summary>
        /// ���������� ��� ��������.
        /// </summary>
        protected async Task AskComplexAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var reply = MessageFactory.Text("�������� ��������� �������� ������:");
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;

            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = new List<CardAction>()
                {
                    new CardAction(){ Title = "���������", Type=ActionTypes.ImBack, Value="���������" },
                    new CardAction(){ Title = "��������", Type=ActionTypes.ImBack, Value="��������" },
                    new CardAction(){ Title = "���������", Type=ActionTypes.ImBack, Value="���������" },
                    new CardAction(){ Title = "���������", Type=ActionTypes.ImBack, Value="���������" },
                    new CardAction(){ Title = "�������", Type=ActionTypes.ImBack, Value="�������" }
                }
            };
            await turnContext.SendActivityAsync(reply);
        }

        /// <summary>
        /// ����� �������.
        /// </summary>
        protected async Task ChooseAdresses(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            string mycomplex = turnContext.Activity.Text;
            switch (mycomplex)
            {
                case "���������":
                    await MyasoAdressesAsync(turnContext, cancellationToken);
                    break;
                case "��������":
                    await PokraAdressesAsync(turnContext, cancellationToken);
                    break;
                case "���������":
                    await ShablaAdressesAsync(turnContext, cancellationToken);
                    break;
                case "���������":
                    await OrdaAdressesAsync(turnContext, cancellationToken);
                    break;
                case "�������":
                    await OrdaAdressesAsync(turnContext, cancellationToken);
                    break;
                default:
                    await DefaultAdressesAsync(turnContext, cancellationToken);
                    break;
            }
        }

        protected async Task DefaultAdressesAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            if (turnContext.Activity.Text == "/start")
            {
                await turnContext.SendActivityAsync(CreateActivityWithTextAndSpeak($"����� �����������:"), cancellationToken);
                cnt = 2;
                await AskComplexAsync(turnContext, cancellationToken);
            }
            else
            {
                var reply = MessageFactory.Text($"�� ����� ���� �����: '{turnContext.Activity.Text}'");
                reply.Type = ActivityTypes.Message;
                reply.TextFormat = TextFormatTypes.Plain;
                await turnContext.SendActivityAsync(reply);

                if (cnt <= 3)
                {
                    adress1 = turnContext.Activity.Text;
                    await turnContext.SendActivityAsync(CreateActivityWithTextAndSpeak($"������ �������� ����� ��������:"), cancellationToken);
                    await AskComplexAsync(turnContext, cancellationToken);
                    cnt++;

                }
                else
                {

                    adress2 = turnContext.Activity.Text;
                    var reply1 = ProcessInput(turnContext);
                    await turnContext.SendActivityAsync(reply1, cancellationToken);
                    await AskComplexAsync(turnContext, cancellationToken);
                    cnt = 2;
                }
            }
            

        }


        // ����� ���������� �������.
        protected async Task MyasoAdressesAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var reply = MessageFactory.Text("�������� ��������� ������:");
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;

            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = new List<CardAction>()
                {
                    new CardAction(){ Title = "������, ��������� ��������, 4", Type=ActionTypes.ImBack, Value="������, ��������� ��������, 4" },
                    new CardAction(){ Title = "������, ������������� ��������, 3", Type=ActionTypes.ImBack, Value="������, ������������� ��������, 3" },
                    new CardAction(){ Title = "������, ���������, 11", Type=ActionTypes.ImBack, Value="������, ���������, 11" },
                    new CardAction(){ Title = "������, ���������, 13", Type=ActionTypes.ImBack, Value="������, ���������, 13" },
                    new CardAction(){ Title = "������, ���������, 18", Type=ActionTypes.ImBack, Value="������, ���������, 18" }
                }
            };
            await turnContext.SendActivityAsync(reply);
        }

        protected async Task PokraAdressesAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var reply = MessageFactory.Text("�������� ��������� ������:");
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;

            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = new List<CardAction>()
                {
                    new CardAction(){ Title = "������, ������� ���������������� ��������, 3", Type=ActionTypes.ImBack, Value= "������, ������� ���������������� ��������, 3"},
                    new CardAction(){ Title = "������, ����� ����������������� ��������, 8/2", Type=ActionTypes.ImBack, Value="������, ����� ����������������� ��������, 8/2" },
                    new CardAction(){ Title = "������, ���������� �������, 11", Type=ActionTypes.ImBack, Value="������, ���������� �������, 11" },
                    new CardAction(){ Title = "������, ���������� ��������, 2/8" , Type=ActionTypes.ImBack, Value="������, ���������� ��������, 2/8" },
                    new CardAction(){ Title = "������, ���������� ��������, 4", Type=ActionTypes.ImBack, Value="������, ���������� ��������, 4" }
                }
            };
            await turnContext.SendActivityAsync(reply);
        }

        protected async Task ShablaAdressesAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var reply = MessageFactory.Text("�������� ��������� ������:");
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;

            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = new List<CardAction>()
                {
                    new CardAction(){ Title = "������, ���������, 26", Type=ActionTypes.ImBack, Value="������, ���������, 26" },
                    new CardAction(){ Title = "������, ���������, 28/11", Type=ActionTypes.ImBack, Value="������, ���������, 28/11" }
                }
            };
            await turnContext.SendActivityAsync(reply);
        }

        protected async Task OrdaAdressesAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var reply = MessageFactory.Text("�������� ��������� ������:");
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;

            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = new List<CardAction>()
                {
                    new CardAction(){ Title = "������, ������ ���������, 21/4" , Type=ActionTypes.ImBack, Value="������, ������ ���������, 21/4" },
                    new CardAction(){ Title = "������, ������� �������, 47/7", Type=ActionTypes.ImBack, Value="������, ������� �������, 47/7" },
                    new CardAction(){ Title = "������, ����� �������, 17", Type=ActionTypes.ImBack, Value="������, ����� �������, 17" },
                    new CardAction(){ Title = "������, ����� �������, 18", Type=ActionTypes.ImBack, Value="������, ����� �������, 18" }
                }
            };
            await turnContext.SendActivityAsync(reply);
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(CreateActivityWithTextAndSpeak($"������, � ���-��������� �����! ������ ���, ���� ������ ������."), cancellationToken);
                }
            }
        }
        private IActivity CreateActivityWithTextAndSpeak(string message)
        {


            var activity = MessageFactory.Text(message);
            string speak = @"<speak version='1.0' xmlns='https://www.w3.org/2001/10/synthesis' xml:lang='en-US'>
              <voice name='Microsoft Server Speech Text to Speech Voice (en-US, JessaRUS)'>" +
              $"{message}" + "</voice></speak>";
            activity.Speak = speak;
            return activity;
        }
    }
}
