﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using BotLib.RoutParseClasses;
namespace BotLib
{
    public static class Search
    {
        public static BotLib.SearchJsonParse.RootObject GetJson(string adress)
        {
            string responseFromServer;

            adress = String.Join('+', adress.Split(" ", StringSplitOptions.RemoveEmptyEntries));
            WebRequest request = WebRequest.Create("https://geocoder.api.here.com/6.2/geocode.json?app_id=WCDkSn40b1i1zYNWcW8R&app_code=-7NAhxCWMr-aLzq_09pECg&searchtext=" + adress);
            WebResponse response = request.GetResponse();
            // Display the status.  
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            using (Stream dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                responseFromServer = reader.ReadToEnd();
                // Display the content.  
            }
            response.Close();
            return JsonConvert.DeserializeObject<BotLib.SearchJsonParse.RootObject>(responseFromServer);
        }

        public static string GetCoords(BotLib.SearchJsonParse.RootObject json)
        {
            string latitude = json.Response.View[0].Result[0].Location.DisplayPosition.Latitude.ToString();
            string longitude = json.Response.View[0].Result[0].Location.DisplayPosition.Longitude.ToString();
            latitude = ParseMethods.ChangeToPoint(latitude);
            longitude = ParseMethods.ChangeToPoint(longitude);
            return latitude + ',' + longitude;
        }
    }
}
