﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using BotLib.RoutParseClasses;

namespace BotLib
{
    public static class ParseMethods
    {
        public static string ChangeToPoint(string coord)
        {
            char[] arr = coord.ToCharArray();
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == ',')
                    arr[i] = '.';
            }
            return new string(arr);
        }

        public static RoutParseClasses.RootObject GetJson(string x, string y)
        {
            string responseFromServer;
            RoutRequest routRequest = new RoutRequest(x, y);
            WebRequest request = WebRequest.Create(routRequest.ToString());
            WebResponse response = request.GetResponse();
            // Display the status.  
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            using (Stream dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                responseFromServer = reader.ReadToEnd();
                // Display the content.  
            }
            response.Close();
            return JsonConvert.DeserializeObject<RootObject>(responseFromServer);
        }

        public static string GetPoints(RoutParseClasses.RootObject json)
        {
            string result = string.Empty;
            var coordsArr = json.response.route[0].leg[0].maneuver;
            Console.WriteLine(coordsArr.Count);
            foreach (var item in coordsArr)
            {
                result += ChangeToPoint(item.position.latitude.ToString()) + "%2C"
                    + ChangeToPoint(item.position.longitude.ToString()) + "%2C";
            }
            return result.Substring(0, result.Length - 3);
        }

        public static string GetStartAndEndPoints(string allCoords)
        {
            string[] coords = allCoords.Split("%2C", StringSplitOptions.RemoveEmptyEntries);
            return coords[0] + "%2C" + coords[1] + "%2C" + coords[coords.Length - 2] + "%2C"
                + coords[coords.Length - 1];
        }

        public static string GetLink(string coords, string startAndEnd)
        {
            return "https://image.maps.api.here.com/mia/1.6/route?r0=" + coords + "&r1=&m0=" + startAndEnd + "&m1=&lc0=00ff00&sc0=000000&lw0=1.5&lc1=ff0000&sc1=0000ff&lw1=3&w=800&app_id=WCDkSn40b1i1zYNWcW8R&app_code=-7NAhxCWMr-aLzq_09pECg";
        }
    }
}
